let amrine_blue = "hsl(213, 96%, 18%)";
let purplish_blue = "hsl(243, 100%, 62%)";
let light_blue = "hsl(206, 94%, 87%)";
let white = "hsl(0, 0%, 100%)";

let pages = document.querySelectorAll(".page");
let stepNumber = document.querySelectorAll(".step_number");
let nextButton = document.querySelector(".next_step");
let backButtonSpan = document.querySelector(".go_back_span");
let backButton = document.querySelector(".go_back");
let requiredMessage = document.querySelectorAll(".required_message");
let inputBoxes = document.querySelectorAll(".input");
let monthly_yearly_change = document.querySelector("#monthly_yearly_change");
let switchYearly = document.querySelector(".yearly");
let switchMonthly = document.querySelector(".monthly");
let selectionButton = document.querySelector(".selection_button");
let planPrices = document.querySelectorAll("price");
let offers = document.querySelectorAll(".offer");
let prices = document.querySelectorAll(".price");
let radioButton = document.querySelectorAll(".radio");

let serviceCost = document.querySelectorAll(".service_cost");
let addOns = document.querySelectorAll(".checkbox");
let selectedAddOnsList = document.querySelectorAll(".selected_add_ons_list");
let selectedAddOns = document.querySelectorAll(".selected_add_ons");
let finalCost = document.querySelector(".final_cost");
let changeButton = document.querySelector(".selected_plan_type_change");
let thankYouPage = document.querySelector(".thank_you_page_container");
let buttonContainer = document.querySelector(".button_container");

let monthly_plan_price = [9, 12, 15];
let yearly_plan_price = [90, 120, 150];
let monthly_service_cost = [1, 2, 2];
let yearly_service_cost = [10, 20, 20];
let addOnsCost = [0, 0, 0];
let planNameList = ["Arcade", "Advance", "Pro"];

let currentPage = 0;
let start = 0;
let selectedPlanIndex = 0;
let planSelectedOrNot = false;

let selectedPlanType = "Monthly";
let selectedPlanTypeShort = "mo";
let selectedPlanName = "Arcade";
let planCost = monthly_plan_price[0];

let plan_price_list = monthly_plan_price;
let service_cost_list = monthly_service_cost;

requiredMessage.forEach((message) => (message.style.display = "none"));
offers.forEach((message) => message.classList.toggle("flex"));

pages[currentPage].style.display = "flex";
stepNumber[currentPage].style.background = light_blue;
stepNumber[currentPage].style.color = "black";
backButtonSpan.style.display = "none";
switchMonthly.classList.toggle("colorAB");
selectedAddOnsList[1].classList.add("flex");
thankYouPage.classList.add("flex");

nextButton.addEventListener("click", nextPagePreview);
backButton.addEventListener("click", backPagePreview);
monthly_yearly_change.addEventListener("click", monthlyYearlySwitch);
addOns.forEach((addOn) => addOn.addEventListener("click", selectAddOn));
selectedAddOns.forEach((selectAddOns) => selectAddOns.classList.add("flex"));
changeButton.addEventListener("click", goOnPlanSelectionPage);

function monthlyYearlySwitch() {
  if (monthly_yearly_change.checked) {
    start = 3;
    selectedPlanType = "Yearly";
    selectedPlanTypeShort = "yr";
    plan_price_list = yearly_plan_price;
    service_cost_list = yearly_service_cost;
    planCost = yearly_plan_price[selectedPlanIndex];
    prices.forEach(
      (price, index) => (price.innerText = "$" + `${plan_price_list[index]}/yr`)
    );
    serviceCost.forEach(
      (cost, index) => (cost.innerText = "$" + `${service_cost_list[index]}/yr`)
    );
  } else {
    start = 0;
    selectedPlanType = "Monthly";
    selectedPlanTypeShort = "mo";
    plan_price_list = monthly_plan_price;
    service_cost_list = monthly_service_cost;
    planCost = monthly_plan_price[selectedPlanIndex];
    prices.forEach(
      (price, index) => (price.innerText = "$" + `${plan_price_list[index]}/mo`)
    );
    serviceCost.forEach(
      (cost, index) => (cost.innerText = "$" + `${service_cost_list[index]}/mo`)
    );
  }
  switchMonthly.classList.toggle("colorAB");
  switchYearly.classList.toggle("colorAB");
  selectionButton.classList.toggle("justify_right");
  offers.forEach((offer) => offer.classList.toggle("flex"));

  addOnsCost = [0, 0, 0];
  addOns.forEach((addOn) => (addOn.checked = false));
  addOns.forEach((addOn) =>
    addOn.parentNode.parentNode.classList.remove("option_selected")
  );

  selectedAddOns.forEach((selectAddOns) => selectAddOns.classList.add("flex"));
  selectedAddOnsList[0].classList.toggle("flex");
  selectedAddOnsList[1].classList.toggle("flex");
}

function nextPagePreview() {
  if (currentPage === 0) {
    if (!firstPageValidation()) {
      return;
    }
  } else if (currentPage === 1) {
    planSelection();
    if (!planSelectedOrNot) {
      return;
    }
  } else if (currentPage === 2) {
    finishingUp();
  } else if (currentPage === 3) {
    thankYouPagePriview();
    return;
  }
  if (currentPage < pages.length) {
    currentPage++;
    if (currentPage === pages.length - 1) {
      nextButton.style.background = purplish_blue;
      nextButton.innerText = "Confirm";
    }
    backButtonSpan.style.display = "flex";
    pages.forEach(pageChange);
  }
}

function planSelection() {
  let index = 0;
  radioButton.forEach((radio) => {
    if (radio.checked) {
      planSelectedOrNot = true;
      index = radio.value;
    }
  });
  selectedPlanIndex = index;
  selectedPlanName = planNameList[index];
  planCost = plan_price_list[index];
}
function selectAddOn() {
  let index = Array.from(addOns).indexOf(this);
  this.parentNode.parentNode.classList.toggle("option_selected");
  selectedAddOns[index + start].classList.toggle("flex");
  if (this.checked) {
    addOnsCost[index] = service_cost_list[index];
  } else {
    addOnsCost[index] = 0;
  }
}

function pageChange(page, index) {
  if (index === currentPage) {
    page.style.display = "flex";
    stepNumber[index].style.background = light_blue;
    stepNumber[index].style.color = "black";
  } else {
    page.style.display = "none";
    stepNumber[index].style.background = "none";
    stepNumber[index].style.color = white;
  }
}

function firstPageValidation() {
  let name = document.getElementById("name").value;
  let email = document.getElementById("email").value;
  let number = document.getElementById("mo_number").value;
  let check = true;

  name.length === 0
    ? ((requiredMessage[0].style.display = "flex"),
      inputBoxes[0].classList.add("border_color"))
    : ((requiredMessage[0].style.display = "none"),
      inputBoxes[0].classList.remove("border_color"));
  email.length === 0
    ? ((requiredMessage[1].style.display = "flex"),
      inputBoxes[1].classList.add("border_color"))
    : ((requiredMessage[1].style.display = "none"),
      inputBoxes[1].classList.remove("border_color"));
  number.length === 0
    ? ((requiredMessage[2].style.display = "flex"),
      inputBoxes[2].classList.add("border_color"))
    : ((requiredMessage[2].style.display = "none"),
      inputBoxes[2].classList.remove("border_color"));

  if (name.length === 0 || email.length === 0 || number.length === 0) {
    check = false;
  }

  return check;
}

function backPagePreview() {
  if (currentPage !== 0) {
    currentPage--;
    if (currentPage === 0) {
      backButtonSpan.style.display = "none";
    }
    if (currentPage === pages.length - 2) {
      nextButton.style.background = amrine_blue;
      nextButton.innerText = "Next Step";
    }
    pages.forEach((page, index) => {
      if (index === currentPage) {
        page.style.display = "flex";
        stepNumber[index].style.background = light_blue;
        stepNumber[index].style.color = "black";
      } else {
        page.style.display = "none";
        stepNumber[index].style.background = "none";
        stepNumber[index].style.color = white;
      }
    });
  }
}

function goOnPlanSelectionPage() {
  currentPage = 1;
  pages.forEach(pageChange);
}

function finishingUp() {
  document.querySelector(
    ".selected_plan_type_name"
  ).innerText = `${selectedPlanName}(${selectedPlanType})`;
  document.querySelector(
    ".selected_plan_cost"
  ).innerText = `$${planCost}/${selectedPlanTypeShort}`;
  finalCost.innerText = `+$${
    addOnsCost.reduce((acc, data) => acc + data, 0) + planCost
  }/${selectedPlanTypeShort}`;
}

function thankYouPagePriview() {
  thankYouPage.classList.remove("flex");
  pages[3].style.display = "none";
  buttonContainer.classList.add("flex");
}
